﻿using System;
using System.ComponentModel.Design;
using System.Linq;

int[] array = {1, 56, 89, 34, 12, 8};

//Buscar

int elementobuscar = 89;

for(int i = 0; i < array.Length; i++)
{
    int elemento = array[i];
    if(elemento == elementobuscar)
        Console.WriteLine(elemento + "pos: " + i);
}

Console.WriteLine();

//Ordenar de menor a mayor

int[] ordenada = new int[array.Length];

int VarActual = int.MinValue; 
int VarAnt = int.MinValue; 

for (int i = 0; i < ordenada.Length; i++)
{
    VarActual = int.MaxValue; 

    for (int j = 0; j < array.Length; j++)
    {
        if (array[j] > VarAnt && array[j] < VarActual) 
        {
            VarActual = array[j];
        }
    }

    ordenada[i] = VarActual;
    VarAnt = VarActual;
}

Console.WriteLine(String.Join(" ", ordenada));

Console.WriteLine();

//Eliminar un valor 

int elementonulo = 56;

for (int i = 0; i < array.Length; i++)
{
    int elemento = array[i];
    if (elemento != elementonulo)
    {
        Console.WriteLine(elemento);
    }
}
